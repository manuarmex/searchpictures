import React from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import Main from '../components/main';
import PicturesList from '../components/picturesList';

class Routes extends React.Component {
    render(){
        return (
           <Router history={hashHistory}>
                <Route path="/" component={Main}>
                    <Route path="/pictures/:tag" component={PicturesList} />
                </Route>
           </Router>
           
        )
    }
}

export default Routes;