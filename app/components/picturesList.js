import React, {Component} from 'react';
import {hashHistory} from "react-router";
import api from '../utils/api';
import PictureInfo from './pictureInfo';
import Pagination from './pagination';
import Rebase from "re-base";

/* Configuration for Firebase database */
const base = Rebase.createClass({
      apiKey: "AIzaSyA-GryBMGpjX4B193bUNJ-RYoBFuWvQNzw",
      authDomain: "search-pictures.firebaseapp.com",
      databaseURL: "https://search-pictures.firebaseio.com"
});
/* Parent node in Database*/
const FBEndpoint = 'pictures';

class PicturesList extends Component {
    constructor(props){
        super(props);
        this.state = {
            pictures : [],
            picturesFavorites : [],
            page : 1,
            totalPages: 0,
            isPictureOpen : false,
            urlToOpen : '',
            titleToOpen : '',
            pictureAuthor : '',
            authorInfo : {}
        }
    }
    componentWillReceiveProps(nextProps){
         this.init(nextProps.params.tag, this.state.page);
    }
    componentDidMount(){    
        this.init(this.props.params.tag, this.state.page);
    }
    init(tag, page){
        if (tag === 'favorites'){
            this.openFavorites()
        } else {
            api.getPictures(tag, page)
                .then(function(data){
                    this.setState({
                        pictures : data.photo,
                        totalPages : data.pages
                    })
                }.bind(this))
                .catch(function(){
                    console.log('Empty data');
                });
        }
    }
    openPictureInfo(url, pic, author) {
        if (!this.state.isPictureOpen){
            this.setState({ 
                isPictureOpen: true,
                urlToOpen : url.replace('_q', '_z'),
                pictureToOpen : pic,
                pictureAuthor :  author
            });    
       
            api.getAuthorInfo(author)
                .then(function(data){
                    this.setState({
                        authorInfo : data,
                    })
                }.bind(this))
                .catch(function(){
                    console.log('Empty data');
                });
         }
    }
    closePictureInfo() {
        this.setState({ isPictureOpen: false });
    }
    addToFavorites(pic) {
        pic.favorite = true;
        this.setState({
            picturesFavorites :this.state.picturesFavorites.concat([pic.id])
        })
        /* Add picture  to  Firebase database*/
        base.post(`${FBEndpoint}/${pic.id}`, {
            data: pic
        })
    }
    removeFromFavorites(pic){
        pic.favorite = false;
        this.setState({
            picturesFavorites :  this.state.picturesFavorites.filter((item) => item !== pic.id)
        })
        /* Remove picture  from  Firebase database */
        base.remove(`${FBEndpoint}/${pic.id}`,function(err){
            if(!err){
                console.log('Deleted picture from database')
                }
            });
        
    }
    openFavorites(){ 
        /* Get pictures saved in Firebase database */
        this.ref = base.bindToState(FBEndpoint,{
            context: this,
            asArray: true,
            state: 'pictures',
            then : function(){
                let favorites = this.state.pictures.map(function(pic){
                    return pic.id
                })
                this.setState({
                    picturesFavorites : favorites,
                    totalPages : 0
                })
            }
        });
    }
    navegateToFavorites(){
        hashHistory.push(`/pictures/favorites`);
    }
    renderPictures(){
        return (
            this.state.pictures.map(function(pic, index){
                // https://www.flickr.com/services/api/misc.urls.html
                let url = `https://farm${pic.farm}.staticflickr.com/${pic.server}/${pic.id}_${pic.secret}_q.jpg`;
                let author = pic.owner;
                let title = pic.title;

                return (
                    <div className="picture-wrapper" key={index} >
                        <object className={url ? 'object-image' : 'nodisplay'} data={url} type={url ? "image/jpeg" : null}
                                onClick={()=> this.openPictureInfo(url, pic, author)}>
                        </object>
                     
                        <div className="picture-label">
                            <span>
                                {/*{!pic.favorite && <i className="glyphicon glyphicon-heart-empty" onClick={()=> this.addToFavorites(pic)}></i>}*/}
                                {pic.favorite && <span className="glyphicon glyphicon-remove-circle" onClick={()=> this.removeFromFavorites(pic)} title="Remove from favorites"></span>}
                            </span>
                        </div>
                    </div>
                )
            }.bind(this))
        )
    }
    changePage(direction){
        if (direction === 'prev'){
            if (this.state.page > 1)
            this.setState({
                page : this.state.page - 1           
            },
            this.init(this.props.params.tag, this.state.page-1))
             
        } else if (direction === 'next'){
            if (this.state.page < this.state.totalPages)
            this.setState({
                page : this.state.page + 1           
            },
            this.init(this.props.params.tag, this.state.page+1))
        }
    }
    render() {
        return (
            <div>
                <div className="favorites" >
                    <span onClick={()=> this.navegateToFavorites()}>See favorites <i className="glyphicon glyphicon-heart"></i></span>
                </div>
                <div className="pictures-container" >
                    {this.renderPictures()}
                </div>
                <Pagination 
                    page={this.state.page} 
                    totalPages={this.state.totalPages}
                    changePage={(direction) => this.changePage(direction)}>
                </Pagination>
                <PictureInfo isOpen={this.state.isPictureOpen} 
                             url={this.state.urlToOpen} 
                             author = {this.state.authorInfo}
                             picture = {this.state.pictureToOpen}
                             picturesFavorites = {this.state.picturesFavorites}
                             close={()=> this.closePictureInfo()}
                             removeFromFavorites={(pic)=> this.removeFromFavorites(pic)}
                             addToFavorites={(pic)=> this.addToFavorites(pic)}>
                </PictureInfo>
            </div>
        );
    }
}

export default PicturesList;