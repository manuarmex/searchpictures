import React, {Component} from 'react';

class Pagination extends Component {
    render() {
        if (this.props.totalPages){
             return (
                <div className="pagination">
                    <button className="prev btn btn-default" onClick={()=> this.props.changePage('prev')}> Prev </button>
                        <span>{this.props.page} of {this.props.totalPages}</span>
                    <button className="next btn btn-default" onClick={()=> this.props.changePage('next')}> Next </button>
                </div>
            );
        } else {
            return null
        }
       
    }
}
Pagination.propTypes = {
    page : React.PropTypes.number,
    totalPages : React.PropTypes.number,
    changePage : React.PropTypes.func
}
export default Pagination;