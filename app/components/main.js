import React from 'react';
import Search from './search';

class Main extends React.Component {
    render(){
        return (
            <div className="main-container">
                <nav className="navbar " role="navigation">
                    <div className="col-sm-8 col-sm-offset-2">
                        <Search />
                    </div>
                </nav>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Main;