import React, {Component} from 'react';
import api from '../utils/api';

class PictureInfo extends Component {
    render() {
                
        let authorThumb = this.props.author.iconfarm ? 
                    `http://farm${this.props.author.iconfarm}.staticflickr.com/${this.props.author.iconserver}/buddyicons/${this.props.author.id}.jpg`
                    : `https://www.flickr.com/images/buddyicon.gif`;
        
        if(this.props.isOpen){
            /* Selected if picture id is imclude in Favorites*/
            let selected = this.props.picturesFavorites.includes(this.props.picture.id);
            return (
                <div className="picture-info">
                    <div className="picture-area">
                        <div className="back" onClick={()=> this.props.close()} ><i className="glyphicon glyphicon-arrow-left"></i>Back to search</div>
                        {this.props.picture.title && <div className="title">{this.props.picture.title}</div>}
                        <img src={this.props.url} alt="Picture"/>
                        <div className="icons">
                            {selected && <span className="glyphicon glyphicon-remove-circle" onClick={()=> this.props.removeFromFavorites(this.props.picture)} title="Remove from favorites"></span>}
                            {!selected && <span className="glyphicon glyphicon-heart-empty" onClick={()=> this.props.addToFavorites(this.props.picture)} title="Add to favorites"></span>}
                            {this.props.author.photosurl && <a href={this.props.author.photosurl['_content']} target="_blank"><span className="glyphicon glyphicon-share-alt" title="Go to photogallery"></span></a>}
                        </div>
                    </div>
                    <div className="author">
                        <div className="thumb-wrapper">
                            <img src={authorThumb} alt="Thumbnail"/>
                        </div>
                        <div className="author-info">
                            {this.props.author.username && <div className="author-name">}
                            {this.props.author.profileurl && <a href={this.props.author.profileurl['_content']} target="_blank"> {this.props.author.username['_content']}</a>}
                                <i className="glyphicon glyphicon-camera"></i> 
                            </div>}
                            {this.props.author.profileurl && <div className="author-link"></div>}
                            {this.props.author.timezone && <div className="author-link"><i>{this.props.author.timezone.label}</i></div>}
                        </div>
                    </div>
                </div>
            );
        } else {
            return null
        }
    }
}

PictureInfo.propTypes = {
    isOpen : React.PropTypes.bool,
    url : React.PropTypes.string,
    picture :React.PropTypes.object,
    picturesFavorites :React.PropTypes.array,
    author : React.PropTypes.object,
    close : React.PropTypes.func,
    addToFavorites : React.PropTypes.func,
    removeFromFavorites : React.PropTypes.func
}
export default PictureInfo;