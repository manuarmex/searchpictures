import React from 'react';
import {hashHistory} from "react-router";

class Search extends React.Component {
    getRef(ref){
        this.pictureTag = ref;
    }
    handleSubmit(){
        const tag = this.pictureTag.value.trim();
        this.pictureTag.value = '';
        hashHistory.push(`/pictures/${tag}`);
    }
    render(){
        return (
            <div className="search col-sm-12">
                <form onSubmit={() => this.handleSubmit()}>
                    <div className="col-sm-8">
                        <input type="text" className="form-control" placeholder="Search your favorites pictures" ref={(ref) => this.getRef(ref)} />
                    </div>
                    <div className="col-sm-4">
                        <button type="submit" className="btn btn-block btn-primary" >Find Pictures</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Search;