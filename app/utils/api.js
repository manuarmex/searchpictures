import axios from 'axios';

let api = (function(){
     const api_key = 'a1f7497dd748fb203ae5172415aed99c';
     const secret = '67228f1549ef97cf';
     
     function getPictures(tag, page) {   
        return axios.get(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${api_key}&tags=${tag}&per_page=35&page=${page}&format=json&nojsoncallback=1`)
                    .then(function(response) { 
                        if (response.data.stat == 'fail'){
                            console.log(response.data.message);
                        }else{
                            return response.data.photos
                        }
                    })
                    .catch(function(){
                        console.log('error geting pictures from API')
                    });
    }
    function getAuthorInfo(authorID) {   
        return axios.get(`https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=${api_key}&user_id=${authorID}&format=json&nojsoncallback=1`)
                    .then(function(response) { 
                        if (response.data.stat == 'fail'){
                            console.log(response.data.message);
                        }else{
                            return response.data.person
                        }
                    })
                    .catch(function(){
                        console.log('error geting pictures from API')
                    });
    }

    return {
        getPictures : getPictures,
        getAuthorInfo : getAuthorInfo
    }
})()
  

export default api;