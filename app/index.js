import React from 'react';
import ReactDom from 'react-dom';
import Routes from './config/routes';

require("../static/css/styles.scss");

ReactDom.render(<Routes />, document.getElementById('app'));
