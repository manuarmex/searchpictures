# README #

React app that search pictures using Flickr API and Firebase database as a learning exercicse

### Functionality ###

* Search pictures using tags
* Open picture and see author details
* Save favorite pictures in to DB (using Firebase database)
* List saved pictures and remove them from DB

### Run it locally ###

- npm install
- npm run start

served in localhost:3333

#### Demo

You can [see a demo here](http://arcemedia.no/searchPictures/)